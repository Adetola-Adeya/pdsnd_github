import time
import pandas as pd
import numpy as np
import datetime as dt

CITY_DATA = { 'chicago': 'chicago.csv',
              'new york city': 'new_york_city.csv',
              'washington': 'washington.csv' }

def get_filters():
    """
    Asks user to specify a city, month, and day to analyze.

    Returns:
        (str) city - name of the city to analyze
        (str) month - name of the month to filter by, or "all" to apply no month filter
        (str) day - name of the day of week to filter by, or "all" to apply no day filter
    """

    print('Hello! Let\'s explore some US bikeshare data!')
    # TO DO: get user input for city (chicago, new york city, washington). HINT: Use a while loop to handle invalid inputs
    cities = ['new york city', 'chicago', 'washington']
    while True:
        city = input('Kindly enter which city you would like to explore: ‘chicago’, ‘new york city’, ‘washington’ \n > ').lower()

        if city not in cities:
            print('invalid input')

        else:
            break

    print('\n')
    # TO DO: get user input for month (all, january, february, ... , june)
    months = ['all', 'january', 'february', 'march', 'april', 'may', 'june']
    month = input('Enter a month filter by, or "all" to apply no month filter\n> {} \n>'.format(months)).lower()


    print('\n')
    # TO DO: get user input for day of week (all, monday, tuesday, ... sunday)
    days = ['all', 'sunday', 'monday', 'tuesday', 'wednesday', 'thursday', 'friday', 'saturday']
    day = input('Enter a month to filter by, or "all" to apply no month filter\n> {} \n>'.format(days)).lower()


    print('\n')


    print('-'*40)
    return city, month, day


def load_data(city, month, day):
    """
    Loads data for the specified city and filters by month and day if applicable.

    Args:
        (str) city - name of the city to analyze
        (str) month - name of the month to filter by, or "all" to apply no month filter
        (str) day - name of the day of week to filter by, or "all" to apply no day filter
    Returns:
        df - Pandas DataFrame containing city data filtered by month and day
    """
    df = pd.read_csv(CITY_DATA[city])# load data file into a dataframe

    df['Start Time'] = pd.to_datetime(df['Start Time'])  # convert the Start Time column to datetime

     # create new columns for month and day of week and hour from Start Time column
    df['month'] = df['Start Time'].dt.month
    df["week_day"] = df['Start Time'].dt.weekday_name
    df['hour'] = df['Start Time'].dt.hour


    if month != 'all':
        months = ['january', 'february', 'march', 'april', 'may', 'june']
        month_index = months.index(month) + 1
        df = df[df['month'] == month_index] #create the new dataframe by filtering by month

    if day != 'all':
        df = df[df["week_day"] == day.title() ]



    return df


def time_stats(df):
    """Displays statistics on the most frequent times of travel."""

    print('\nCalculating The Most Frequent Times of Travel...\n')
    start_time = time.time()

    # TO DO: display the most common month
    most_common_month = df['month'].value_counts().idxmax()
    print("The most common month is: {}".format(most_common_month))


    print('\n')

    # TO DO: display the most common day of week
    most_common_dayofweek = df['week_day'].value_counts().idxmax()
    print("The most common day of week is: {}".format(most_common_dayofweek))


    print('\n')

    # TO DO: display the most common start hour
    most_common_hour = df['hour'].value_counts().idxmax()
    print("The most common hour is: {}".format(most_common_hour))


    print('\n')

    print("\nThis took %s seconds." % (time.time() - start_time))
    print('-'*40)


def station_stats(df):
    """Displays statistics on the most popular stations and trip."""

    print('\nCalculating The Most Popular Stations and Trip...\n')
    start_time = time.time()

    # TO DO: display most commonly used start station
    most_common_start_station = df['Start Station'].value_counts().idxmax()
    print("The most commonly used start station is: {}".format(most_common_start_station))



    print('\n')

    # TO DO: display most commonly used end station
    most_common_end_station = df['End Station'].value_counts().idxmax()
    print("The most commonly used end station is: {}".format(most_common_end_station))


    print('\n')

    # TO DO: display most frequent combination of start station and end station trip
    most_common_start_end_station = df.groupby(['Start Station','End Station']).count().sort_values(by=['Start Station','End Station'],axis = 0).iloc[0]



    print('\n')

    print("\nThis took %s seconds." % (time.time() - start_time))
    print('-'*40)


def trip_duration_stats(df):
    """Displays statistics on the total and average trip duration."""

    print('\nCalculating Trip Duration...\n')
    start_time = time.time()

    # TO DO: display total travel time
    total_travel_time = df["Trip Duration"].sum()
    print("Total time of travel: {}".format(total_travel_time))


    print('\n')

    # TO DO: display mean travel time
    mean_travel_time = df["Trip Duration"].mean()
    print("The mean travel time: {}".format(mean_travel_time))


    print('\n')

    print("\nThis took %s seconds." % (time.time() - start_time))
    print('-'*40)


def user_stats(df):
    """Displays statistics on bikeshare users."""

    print('\nCalculating User Stats...\n')
    start_time = time.time()

    # TO DO: Display counts of user types
    user_types = df['User Type'].value_counts()
    print("Count of user types:{}".format(user_types))

    print('\n')

    # TO DO: Display counts of gender
    if 'Gender' not in df.columns:
          print('Gender input not available')
    else:
        gender = df['Gender']
        gender_count = gender.value_counts()
        female_gender = gender_count["Female"]
        print("Female persons: {}".format(female_gender))
        print('\n')

        gender = df['Gender']
        gender_count = gender.value_counts()
        male_gender = gender_count["Male"]
        print("Male persons: {}".format(male_gender))
        print('\n')


    # TO DO: Display earliest, most recent, and most common year of birth
    if 'Birth Year' not in df.columns:
          print('Birth year not available.')
    else:
          earliest_birthyear = df["Birth Year"].min()
          print("Earliest birth year: {}".format(earliest_birthyear))
          print('\n')
          most_recent_birthyear = df["Birth Year"].max()
          print("Most recent birth year: {}".format(most_recent_birthyear))
          print('\n')
          most_common_birthyear = df["Birth Year"].value_counts().idxmax()
          print("Most common birth year: {}".format(most_common_birthyear))
          print('\n')

    print("\nThis took %s seconds." % (time.time() - start_time))
    print('-'*40)


def display_rawdata(df):
    """Displays raw bikeshare data and
    5 rows will added in each press"""

    print('press enter to see raw data. To skip, press no')
    i = 0
    while (input() != 'no'):
        i = i + 5
        print(df.head(i))


def main():
    while True:
        city, month, day = get_filters()
        df = load_data(city, month, day)

        time_stats(df)
        station_stats(df)
        trip_duration_stats(df)
        user_stats(df)
        display_rawdata(df)

        restart = input('\nWould you like to restart? Enter yes or no.\n')
        if restart.lower() != 'yes':
            break


if __name__ == "__main__":
	main()
